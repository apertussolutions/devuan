#!/bin/sh -eu
# trim.sh -- some flab of a Devuan base image
# Copyright (C) 2017  Olaf Meeuwissen
#
# License: GPL-3.0+

DEBIAN_FRONTEND=noninteractive
export DEBIAN_FRONTEND

suite=$1
rootfs=$2-$suite
cp -al $rootfs $rootfs-slim
rootfs=$rootfs-slim

mount -t devpts devpts $rootfs/dev/pts

bindir=usr/local/bin
install -m 0755 -o root -g root scripts-slim/* $rootfs/$bindir

for script in $(ls $rootfs/$bindir); do
    echo "+ /$bindir/$script"
    chroot $rootfs /$bindir/$script $suite
done

chroot $rootfs apt-get --purge autoremove -q -y
chroot $rootfs /$bindir/docker-apt-clean $suite

umount $rootfs/dev/pts

tar -caf $rootfs.tar.gz \
    --directory $rootfs \
    --exclude './dev/**' \
    --numeric-owner \
    --transform 's,^\./,,' .
